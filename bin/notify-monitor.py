#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import operator
import os
import smtplib
import errno
import socket
from time import sleep
from email.utils import formatdate
from email.mime.text import MIMEText


def send_email(to, subject, text, sender="notification@localhost", charset='utf8', server='localhost'):
    msg = MIMEText(text)
    msg.set_charset(charset)
    msg['From'] = sender
    msg['To'] = to
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    smtp = smtplib.SMTP(server)
    smtp.sendmail(sender, to, msg.as_string())
    smtp.close()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--email",
                        help="Email to notify once condition is met")
    parser.add_argument("--subject",
                        default="Notification conditions met on {}".format(socket.gethostname()),
                        help="Notification email subject")
    parser.add_argument("--condition", choices=["all", "any"], default="all",
                        help="Notify when all/any condition is met (default: all)")
    parser.add_argument("-c", "--checkevery", type=int, default=1,
                        help="Number of seconds to wait between checks")

    # Load
    parser.add_argument("-l1", "--minload1", type=float,
                        help="Notify when 1min system load is below given value")
    parser.add_argument("-L1", "--maxload1", type=float,
                        help="Notify when 1min system load is above given value")

    parser.add_argument("-l5", "--minload5", type=float,
                        help="Notify when 5min system load is below given value")
    parser.add_argument("-L5", "--maxload5", type=float,
                        help="Notify when 5min system load is above given value")

    parser.add_argument("-l15", "--minload15", type=float,
                        help="Notify when 15min system load is below given value")
    parser.add_argument("-L15", "--maxload15", type=float,
                        help="Notify when 15min system load is above given value")

    # Mem
    parser.add_argument("-m", "--minmem", type=int,
                        help="Notify when memory availability is below given value (in MB)")
    parser.add_argument("-M", "--maxmem", type=int,
                        help="Notify when memory availability is above given value (in MB)")

    # Pid
    parser.add_argument("-p", "--pid", type=int,
                        help="Notify when PID is no longer active")

    args = parser.parse_args()

    if not any((args.minload1, args.maxload1,
                args.minload5, args.maxload5,
                args.minload15, args.maxload15,
                args.minmem, args.maxmem,
                args.pid)):
        parser.error('No monitoring action requested, at least one of --*load*, --*mem or --pid is required')

    if args.condition == "all":
        args.condition = all
        args.condition_msg = ' and '
    else:
        args.condition = any
        args.condition_msg = ' or '

    return args


def monitor_load(action, interval, limit):
    if action(os.getloadavg()[interval], limit):
        return True


def monitor_mem(action, limit):
    if action(get_mem(), limit):
        return True


def monitor_pid(pid):
    "Returns False while the process is active, True otherwise"
    try:
        os.kill(pid, 0)
    except OSError as e:
        if e.errno == errno.ESRCH:
            # The only error that we should treat as inactive PID is if the PID
            # doesn't exist. If it belongs to a different user we might still
            # want to monitor it and that fails with a permission error
            return True
        else:
            return False
    else:
        return False


def get_mem(name="MemAvailable:"):
    with open("/proc/meminfo") as fh:
        for line in fh:
            info = line.rstrip("\n").split()
            if info[0] == name:
                return int(info[1]) / 1024  # We read Kb and need to return Mb


def monitor(args):
    checks = []
    checks_msg = []

    if args.minload1 is not None:
        checks.append((monitor_load, (operator.lt, 0, args.minload1)))
        checks_msg.append("1 minute load is below {}".format(args.minload1))
    if args.maxload1 is not None:
        checks.append((monitor_load, (operator.gt, 0, args.maxload1)))
        checks_msg.append("1 minute load is above {}".format(args.maxload1))

    if args.minload5 is not None:
        checks.append((monitor_load, (operator.lt, 1, args.minload5)))
        checks_msg.append("5 minute load is below {}".format(args.minload5))
    if args.maxload5 is not None:
        checks.append((monitor_load, (operator.gt, 1, args.maxload5)))
        checks_msg.append("5 minute load is above {}".format(args.maxload5))

    if args.minload15 is not None:
        checks.append((monitor_load, (operator.lt, 2, args.minload15)))
        checks_msg.append("15 minute load is below {}".format(args.minload15))
    if args.maxload15 is not None:
        checks.append((monitor_load, (operator.gt, 2, args.maxload15)))
        checks_msg.append("15 minute load is above {}".format(args.maxload15))

    if args.minmem is not None:
        checks.append((monitor_mem, (operator.lt, args.minmem)))
        checks_msg.append("memory usage is below {}MB".format(args.minmem))
    if args.maxmem is not None:
        checks.append((monitor_mem, (operator.gt, args.maxmem)))
        checks_msg.append("memory usage is above {}MB".format(args.maxmem))

    if args.pid is not None:
        checks.append((monitor_pid, (args.pid,)))
        checks_msg.append("pid {} is no longer active".format(args.pid))

    msg = args.condition_msg.join(checks_msg)

    if args.email is None:
        print("Sending bell notification when:", msg)
    else:
        print("Sending email notification when:", msg)

    unmet_conditions = True

    while unmet_conditions:
        state = []
        for check, arguments in checks:
            state.append(check(*arguments))

        if args.condition(state):
            unmet_conditions = False

        if unmet_conditions:
            sleep(args.checkevery)

    bell = '\a'

    if args.email is None:
        print("Conditions met. No email set. Doing bell notification", bell)
    else:
        print("Conditions met. Sending email notification", bell)
        send_email(args.email, args.subject, "\n".join(checks_msg))


if __name__ == "__main__":
    args = parse_args()
    monitor(args)
