#!/usr/bin/env sh
# docker_wrap.sh -- created 2017-02-09 - Renato Alves

usage() {
    echo >&2 "This command launches a docker container automatically mounting"
    echo >&2 "the user's home folder on /remote and creating a user to ensure"
    echo >&2 "compatible execution permissions"
    echo >&2 ""
    echo >&2 "Usage:"
    echo >&2 "    $0 [options] docker_image"
    echo >&2 ""
    exit 1
}

setup_user() {
    NEWUSER="dockeruser"
    NEWGROUP="$NEWUSER"
    NEWUID="$(stat -c '%u' "$TARGET")"
    NEWGID="$(stat -c '%g' "$TARGET")"

    HAVE_BUSYBOX="$(which busybox)"

    if [ -f "/bin/zsh" ]; then
        USE_SHELL="/bin/zsh"
    elif [ -f "/bin/bash" ]; then
        USE_SHELL="/bin/bash"
    else
        USE_SHELL="/bin/sh"
    fi

    echo ">>> checking for group $NEWGID"
    if ! getent group "$NEWGID" >/dev/null 2>/dev/null ; then
        if [ ! -z "$HAVE_BUSYBOX" ]; then
            busybox addgroup -g "$NEWGID" "$NEWUSER" || (echo "!!! Couldn't create group with id $NEWGID" && exit 1)
        else
            addgroup --gid "$NEWGID" "$NEWUSER" || (echo "!!! Couldn't create group with id $NEWGID" && exit 1)
        fi
        echo ">>> created group $NEWGROUP($NEWGID)"
    else
        NEWGROUP="$(getent group "$NEWGID" | cut -d: -f1)"
        echo "~~~ group $NEWGROUP($NEWGID) already exists"
    fi
    echo ">>> checking for user $NEWUID"
    if ! getent passwd "$NEWUID" >/dev/null 2>/dev/null ; then
        if [ ! -z "$HAVE_BUSYBOX" ]; then
            busybox adduser -G "$NEWGROUP" -u "$NEWUID" -h "$TARGET" -s "$USE_SHELL" -D "$NEWUSER" || (echo "!!! Couldn't create user" && exit 1)
        else
            adduser --ingroup "$NEWGROUP" --uid "$NEWUID" --no-create-home --home "$TARGET" --shell "$USE_SHELL" --gecos "" --disabled-password "$NEWUSER" || (echo "!!! Couldn't create user" && exit 1)

        fi
        echo ">>> created user $NEWUSER($NEWUID)"
    else
        NEWUSER="$(getent passwd "$NEWUID" | cut -d: -f1)"
        echo "~~~ user $NEWUSER($NEWUID) already exists"
    fi
    if [ ! -z "$HAVE_BUSYBOX" ]; then
        echo "root:r" | busybox chpasswd
    else
        echo "root:r" | chpasswd
    fi
    echo ">>> password set on the root user"
    echo ">>> use 'su -' and password 'r' to become super-user"

    if [ -f "/usr/bin/script" ]; then
        script --quiet --return -c "/bin/su - '$NEWUSER'"
    else
        chmod u+s /bin/su
        exec /bin/su - "$NEWUSER"
    fi
}

[ "x$1" = "x" ] && usage

# Get the name of the image filtering for allowed characters and use it as hostname later
IMAGE="$(for last in "$@"; do : ; done; echo "$last" | tr ':' '-' | tr -cd 'A-Za-z0-9_-')"
TARGET="/remote"
WRAPPER_OPTION="--setup-docker-wrapper"

if [ "$1" = "$WRAPPER_OPTION" ]; then
    setup_user
else
    USERHOME="$(readlink -f ~)"
    WRAPPER_LOCAL="$(readlink -f "$0")"
    WRAPPER="${TARGET}${WRAPPER_LOCAL#$USERHOME}"
    # Ensure we managed to get a valid location for the wrapper script
    if [ "$WRAPPER" != "${TARGET}/$(basename "$0")" ]; then
        if [ "$WRAPPER" = "${TARGET}${WRAPPER_LOCAL}" ]; then
            echo "ERROR: Unexpected location for $0 inside the container."
            echo "  Obtained: $WRAPPER"
            echo "For this to work $0 needs to be inside the user's home folder"
            exit 1
        fi
    fi
    if [ -d /data ]; then
        DATAVOLUME="-v /data:/data:rw"
    else
        DATAVOLUME=
    fi
    docker run -it -h "$IMAGE" $DATAVOLUME -v "$USERHOME":"$TARGET":rw "$@" "$WRAPPER" "$WRAPPER_OPTION"
fi
