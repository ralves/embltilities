#!/usr/bin/env bash

./clear.sh

for t in *.test; do
    echo "Submitting $t"
    ../bin/submit-sge -a 1 -m .128 -k 00:01:00 -n "$t" -l "$t.log" "$t"
done

