#!/usr/bin/env bash

FAILS=0

for t in *.test; do
    if [ ! -f "${t}.result" ]; then
        echo "${t}.result doesn't exist"
        FAILS=1
        continue
    fi

    if ! diff -u "${t}.expected" "${t}.result" ; then
        echo "${t} result is different than expected"
        FAILS=1
    fi

    if [ -s "${t}.log" ]; then
        echo "Warning ${t}.log isn't empty"
    fi
done

if [ $FAILS -eq 0 ]; then
    echo "All passed"
fi

exit $FAILS

