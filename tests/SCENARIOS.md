Scenarios that need to be tested

tests:
    pipes
    single quotes
    double quotes
    comments
    precmd variables
    inline variables
    continuation AND (&&) OR (||) and (;)

locations:
    precmd
    normal
    expanded

combination of all in a single run
